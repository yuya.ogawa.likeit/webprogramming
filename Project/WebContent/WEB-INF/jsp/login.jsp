<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>login</title>

    <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">

</head>
<body>

     <header>
         <div align="center">
      <h1>ログイン画面</h1>
         </div>
    </header>

    <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

<form  action="LoginServlet" method="post">
    <div class="row justify-content-center">
    <div class="col-4">
    <div class="form-group">
    <div class="form-row">

            <label for="exampleInputLogin">ログイン ID</label>
            <input type="text" class="form-control" id="inputLoginId" name="loginId"  placeholder="login">
            <label for="exampleInputPassword1">パスワード</label>
            <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password">

    </div>
    </div>
    </div>
    </div>

    <div align="center">
        <button type="submit" class="btn btn-primary">ログイン</button>
    </div>
 </form>
</body>
</html>


