<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
        <title>title</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>


    <header>
          <div class="container">
            <div class="row">
            <div class="col-auto mr-auto"></div>
                <div class="col-auto">
                    ${userInfo.name} さん
                    <a href="LoginServlet">ログアウト</a>
                </div>
            </div>
            </div>
    </header>

    <div class="container">
            <div class="row">
            <div class="col-auto mr-auto"></div>
                <div class="col-auto">
        <a href="RegistrationServlet">新規登録</a>
      </div>
      </div>
      </div>

<form method="post" action="UserListServlet">

    <div align="center">
        <h1> ユーザ一覧 </h1>
    </div>

    <div class="row justify-content-center">
    <div class="col-4">
    <div class="form-group">
    <div class="form-row">
            <label for="exampleInputLogin">ログイン ID</label>
            <input type="text" class="form-control" name="loginId"  placeholder="login">
    </div>
    </div>
    </div>
    </div>

        <div class="row justify-content-center">
        <div class="col-4">
        <div class="form-group">
        <div class="form-row">
            <label for="inputUserName">ユーザー名</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="UserName">
        </div>
        </div>
        </div>
        </div>

        <div class="row justify-content-center">
        <div class="form-row">
        <div class="form-group">
            <label for="inputbday"> 生年月日 </label>
            <input type="date" name="birthDate">
              ~
            <input type="date" name="birthDate2">
        </div>
        </div>
        </div>

        <div class="row justify-content-center">
        <div class="form-group">
        <div class="form-row">
            <button type="submit" class="btn btn-primary"> 検索 </button>
        </div>
        </div>
        </div>

        </form>

        <div class="row justify-content-center">
        <div class="col-8">
        <div class="form-group">
        <div class="form-row">
        <table class="table table-bordered">
            <tr>
                <td>
                    ログインID
                </td>
                <td>
                    ユーザー名
                </td>
                <td>
                    生年月日
                </td>
                <td>
                </td>
            </tr>

             <c:forEach var="user" items="${userList}" >
                   <tr>

                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>

                     <td>

                       <a class="btn btn-primary" href="DetailsServlet?id=${user.id}">詳細</a>

                        <c:if test="${userInfo.name == '管理者' || userInfo.name == user.name}">
                       <a class="btn btn-success" href="UpdateServlet?id=${user.id}">更新</a>
                       </c:if>

                        <c:if test="${userInfo.name == '管理者'}">
                       <a class="btn btn-danger" href ="DeleteServlet?id=${user.id}">削除</a>
                     </c:if>

                     </td>
                   </tr>
                 </c:forEach>
        </table>
        </div>
        </div>
        </div>
        </div>

    <footer>

            </div>
            </div>

            </footer>


</body>
</html>
