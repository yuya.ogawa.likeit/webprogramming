<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>userSignup</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
</head>
	<body>

        <header>

            <div class="container">
            <div class="row">
            <div class="col-auto mr-auto"></div>
                <div class="col-auto">
                    ${userInfo.name}さん
                    <a href="LoginServlet">ログアウト</a>
                </div>
            </div>
            </div>


        </header>



            <div align="center">
            <h1>ユーザー新規登録</h1>
            </div>

            <c:if test="${errMsg != null}" >
	    		<div class="alert alert-danger" role="alert">
		  			${errMsg}
				</div>
			</c:if>

			<form action="RegistrationServlet" method="post">

            <div class="row justify-content-center">
            <div class="col-4">
            <div class="form-group">
            <div class="form-row">
                 <label for="exampleInputLogin">ログイン ID</label>
                 <input type="text" class="form-control" name="loginId"  placeholder="login">
            </div>
            </div>
            </div>
            </div>

            <div class="row justify-content-center">
            <div class="col-4">
            <div class="form-group">
            <div class="form-row">
                <label for="signupPassword">パスワード</label>
                <input type="password" class="form-control" name="password" placeholder="Password">
            </div>
            </div>
            </div>
            </div>

            <div class="row justify-content-center">
            <div class="col-4">
            <div class="form-group">
            <div class="form-row">
                <label for="signupPassword">パスワード（確認）</label>
                <input type="password" class="form-control" name="password2" placeholder="Password">
            </div>
            </div>
            </div>
            </div>

            <div class="row justify-content-center">
            <div class="col-4">
            <div class="form-group">
            <div class="form-row">
                <label for="inputUserName">ユーザー名</label>
                <input type="text" class="form-control" name="name" id="inputUserName">
            </div>
            </div>
            </div>
            </div>

            <div class="row justify-content-center">
            <div class="col-4">
            <div class="form-group">
            <div class="form-row">
                <label for="inputbday">生年月日</label>
                <input type="text" class="form-control" name="birthDate" id="bday">
            </div>
            </div>
            </div>
            </div>

            <div align="center">
                 <button type="submit" class="btn btn-primary"> 登録 </button>
            </div>

            <footer>

            <div class="container">
            <div class="row">
                <div class="col-auto mr-auto">
                  <a href="UserListServlet">[戻る]</a>
                </div>

            </div>
            </div>

            </footer>

</form>
	</body>
</html>






