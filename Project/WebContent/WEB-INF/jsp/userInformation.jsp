<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>userInformation</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
</head>
	<body>

        <header>

            <div class="container">
            <div class="row">
            <div class="col-auto mr-auto"></div>
                <div class="col-auto">
                     ${userInfo.name}さん
                    <a href="LoginServlet">ログアウト</a>
                </div>
            </div>
            </div>


        </header>

		<form>

            <div align="center">
            <h1>ユーザー情報詳細参照</h1>
            </div>

            <div class="row justify-content-center">
            <div class="col-4">
            <div class="form-group">
            <div class="form-row">
                 <label for="exampleInputLogin">ログイン ID</label>
                 <br>${user.loginId}
            </div>
            </div>
            </div>
            </div>

            <div class="row justify-content-center">
            <div class="col-4">
            <div class="form-group">
            <div class="form-row">
                <label for="inputUserName">ユーザー名</label>
                <br>${user.name}
            </div>
            </div>
            </div>
            </div>

            <div class="row justify-content-center">
            <div class="col-4">
            <div class="form-group">
            <div class="form-row">
                <label for="inputbday">生年月日</label>
                <br>${user.birthDate}
            </div>
            </div>
            </div>
            </div>

            <div class="row justify-content-center">
            <div class="col-4">
            <div class="form-group">
            <div class="form-row">
                <label for="inputbday">登録日時</label>
                <br>${user.createDate}
            </div>
            </div>
            </div>
            </div>

            <div class="row justify-content-center">
            <div class="col-4">
            <div class="form-group">
            <div class="form-row">
                <label for="inputbday">更新日時</label>
                <br>${user.updateDate}
            </div>
            </div>
            </div>
            </div>


            <footer>

            <div class="container">
            <div class="row">
                <div class="col-auto mr-auto">
                   <a href="UserListServlet">[戻る]</a>
                </div>

            </div>
            </div>

            </footer>

</form>
	</body>
</html>






