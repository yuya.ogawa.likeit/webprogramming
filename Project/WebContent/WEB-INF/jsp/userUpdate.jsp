<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>userUpdate</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>

	<header>

		<div class="container">
			<div class="row">
				<div class="col-auto mr-auto"></div>
				<div class="col-auto">
					${userInfo.name}さん <a href="LoginServlet">ログアウト</a>
				</div>
			</div>
		</div>

	</header>



	<form action="UpdateServlet" method="post">
		<input type="hidden" name="id" class="form-control" id="inputUserId"
			value="${id}">
			<input type="hidden" name="loginId" class="form-control" id="inputUserId"
			value="${loginId}">
		<div align="center">
			<h1>ユーザー情報更新</h1>
		</div>
		<c:if test="${errMsg != null}" >
	    		<div class="alert alert-danger" role="alert">
		  			${errMsg}
				</div>
			</c:if>

		<div class="row justify-content-center">
			<div class="col-4">
				<div class="form-group">
					<div class="form-row">
						<label for="exampleInputLogin">ログイン ID</label> <br>${loginId}
					</div>
				</div>
			</div>
		</div>

		<div class="row justify-content-center">
			<div class="col-4">
				<div class="form-group">
					<div class="form-row">
						<label for="signupPassword">パスワード</label> <input type="password"
							class="form-control" placeholder="Password" name = "password">
					</div>
				</div>
			</div>
		</div>

		<div class="row justify-content-center">
			<div class="col-4">
				<div class="form-group">
					<div class="form-row">
						<label for="signupPassword">パスワード（確認）</label> <input
							type="password" class="form-control" placeholder="Password" name = "password2">
					</div>
				</div>
			</div>
		</div>

		<div class="row justify-content-center">
			<div class="col-4">
				<div class="form-group">
					<div class="form-row">
						<label for="inputUserName">ユーザー名</label> <input type="text"
							name="name" class="form-control" id="inputUserName"
							value="${name}">
					</div>
				</div>
			</div>
		</div>

		<div class="row justify-content-center">
			<div class="col-4">
				<div class="form-group">
					<div class="form-row">
						<label for="inputbday">生年月日</label> <input type="text"
							class="form-control" id="bday" value="${birthDate}" name = "birthDate">
					</div>
				</div>
			</div>
		</div>

		<div align="center">
			<button type="submit" class="btn btn-primary">更新</button>
		</div>

		<footer>

			<div class="container">
				<div class="row">
					<div class="col-auto mr-auto">
						<a href="UserListServlet">[戻る]</a>
					</div>

				</div>
			</div>

		</footer>

	</form>
</body>
</html>






