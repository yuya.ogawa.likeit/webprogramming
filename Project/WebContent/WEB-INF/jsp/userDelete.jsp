<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>userDelete</title>
           <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">
</head>
	<body>

        <header>

            <div class="container">
            <div class="row">
            <div class="col-auto mr-auto"></div>
                <div class="col-auto">
                    ${userInfo.name}さん
                    <a href="LoginServlet">ログアウト</a>
                </div>
            </div>
            </div>


        </header>

		<form action="DeleteServlet" method="post">

<input type="hidden" name="id" class="form-control" id="inputUserId"
			value="${user.id}">

            <div align="center">
            <h1>ユーザー削除確認</h1>
            </div>

            <div class="row justify-content-center">
            <div class="col-4">
            <div class="form-group">
            <div class="form-row">
                 <label for="exampleInputLogin">ログイン ID</label>
                 <br>${user.loginId}
                 <br>を本当に削除してよろしいでしょうか。
            </div>
            </div>
            </div>
            </div>



            <div align="center">
                 <INPUT type="button" class="btn btn-primary" value="キャンセル" onClick="history.back()">
                 <!-- <button type="submit" class="btn btn-primary"> キャンセル </button> -->
                　　
                 <button type="submit" class="btn btn-primary"> 削除 </button>

            </div>

        <footer>

        </footer>

</form>
	</body>
</html>






