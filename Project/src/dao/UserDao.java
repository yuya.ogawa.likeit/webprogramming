package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;


/**
 * ユーザテーブル用のDao
 *
 *
 */
public class UserDao {

    /**
     * ログインIDとパスワードに紐づくユーザ情報を返す
     * @param loginId
     * @param password
     * @return
     */
    public User findByLoginInfo(String loginId, String password) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, password);
            ResultSet rs = pStmt.executeQuery();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            return new User(loginIdData, nameData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }


    /**
     * 全てのユーザ情報を取得する
     * @return
     */
    public List<User> findAll() {
        Connection conn = null;
        List<User> userList = new ArrayList<User>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            // TODO: 未実装：管理者以外を取得するようSQLを変更する
            String sql = "SELECT * FROM user WHERE name not in ('管理者')";

             // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }


    /**
	 * 顧客情報を登録する処理
	 */
	public void createUser(String loginId, String name, String birthDate, String password, String createDate, String updateDate) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベース接続
			con = DBManager.getConnection();

			// 実行SQL文字列定義
			String insertSQL = "INSERT INTO user(login_id,name,birth_date,password,create_date,update_date)VALUES(?,?,?,?,now(),now())";

			// ステートメント生成
			stmt = con.prepareStatement(insertSQL);

			// SQLの?パラメータに値を設定

			stmt.setString(1, loginId);
			stmt.setString(2, name);
			stmt.setString(3, birthDate);
			stmt.setString(4, password);


			// 登録SQL実行
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * ユーザー情報詳細処理
	 */

public User findById(int id) {
    Connection conn = null;
    try {
        // データベースへ接続
        conn = DBManager.getConnection();

        // SELECT文を準備
        String sql = "SELECT * FROM user WHERE id = ? ";

         // SELECTを実行し、結果表を取得
        PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setInt(1, id);

        ResultSet rs = pStmt.executeQuery();


        // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
        if (!rs.next()) {
            return null;
        }

        // 必要なデータのみインスタンスのフィールドに追加
        int IdData = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        String createDate = rs.getString("create_date");
        String updateDate = rs.getString("update_date");


        return new User(IdData,loginId, name, birthDate, password, createDate, updateDate);

    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
  }
/**
 * 顧客情報を更新する処理
 */
public void updateUser(String name, String password, String birthDate, String updateDate, int id) {
	Connection con = null;
	PreparedStatement stmt = null;

	try {
		// データベース接続
		con = DBManager.getConnection();

		// 実行SQL文字列定義
		String insertSQL = "UPDATE user SET name = ?, password = ?, birth_date = ?, update_date = NOW() WHERE id = ?";

		// ステートメント生成
		stmt = con.prepareStatement(insertSQL);

		// SQLの?パラメータに値を設定

		stmt.setString(1, name);
		stmt.setString(2, password);
		stmt.setString(3, birthDate);
		stmt.setInt(4, id);



		// 登録SQL実行
		stmt.executeUpdate();

	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		try {
			// ステートメントインスタンスがnullでない場合、クローズ処理を実行
			if (stmt != null) {
				stmt.close();
			}
			// コネクションインスタンスがnullでない場合、クローズ処理を実行
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
/**
 * 顧客情報を削除する処理
 */
public void delUser(int id) {
	Connection con = null;
	PreparedStatement stmt = null;

	try {
		// データベース接続
		con = DBManager.getConnection();

		// 実行SQL文字列定義
		String insertSQL = "DELETE  FROM user WHERE id = ? ";

		// ステートメント生成
		stmt = con.prepareStatement(insertSQL);

		// SQLの?パラメータに値を設定

		stmt.setInt(1, id);

		// 登録SQL実行
		stmt.executeUpdate();

	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		try {
			// ステートメントインスタンスがnullでない場合、クローズ処理を実行
			if (stmt != null) {
				stmt.close();
			}
			// コネクションインスタンスがnullでない場合、クローズ処理を実行
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
/**
 * ログインID詳細処理
 */

public User findByUserId(String loginId) {
Connection conn = null;
try {
    // データベースへ接続
    conn = DBManager.getConnection();

    // SELECT文を準備
    String sql = "SELECT * FROM user WHERE login_id = ? ";

     // SELECTを実行し、結果表を取得
    PreparedStatement pStmt = conn.prepareStatement(sql);
    pStmt.setString(1, loginId);

    ResultSet rs = pStmt.executeQuery();


    // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
    if (!rs.next()) {
        return null;
    }

    // 必要なデータのみインスタンスのフィールドに追加
    int IdData = rs.getInt("id");
    String loginid = rs.getString("login_id");
    String name = rs.getString("name");
    Date birthDate = rs.getDate("birth_date");
    String password = rs.getString("password");
    String createDate = rs.getString("create_date");
    String updateDate = rs.getString("update_date");


    return new User(IdData,loginid, name, birthDate, password, createDate, updateDate);

} catch (SQLException e) {
    e.printStackTrace();
    return null;
} finally {
    // データベース切断
    if (conn != null) {
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
}

/**
 * 暗号化するメソッド
 */
	public String pass(String password) {


		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes;

		String result = "";

		try {
			bytes = MessageDigest.getInstance(algorithm).digest(password.getBytes(charset));
			 result = DatatypeConverter.printHexBinary(bytes);



		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return result;

	}

	 /**
     * ユーザ情報を検索する
     * @return
     */
    public List<User> findloginIdSearch(String loginId, String name, String birthDate ,String birthDate2) {
        Connection conn = null;
        List<User> userList = new ArrayList<User>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備

            String sql = "SELECT * FROM user WHERE name not in ('管理者')";

            if (!loginId.equals("")) {
            	sql += " AND login_id = '" + loginId + "'";
			}

	          if (!name.equals("")) {
	        	sql += "AND name LIKE '%" + name + "%'";
			}

	          if (!birthDate.equals("")) {
	            sql += "AND birth_date >= '" + birthDate + "'";
			}

	          if(!birthDate2.equals("")) {
	        	  sql += "AND birth_date <= '" + birthDate2 + "'";
	          }

	            // SELECTを実行し、結果表を取得
	            Statement stmt = conn.createStatement();
	            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                int id = rs.getInt("id");
                String loginID = rs.getString("login_id");
                String NAME = rs.getString("name");
                Date BirthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id, loginID, NAME, BirthDate, password, createDate, updateDate);

                userList.add(user);

            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }

}

