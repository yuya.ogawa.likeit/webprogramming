package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UpdateServlet
 */
@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// マルチバイト文字列の文字化け対応
		request.setCharacterEncoding("UTF-8");

		// URLパラメータから値を取得
		String userID = request.getParameter("id");
		int id = Integer.parseInt(userID);

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDao userDao = new UserDao();
		User user = userDao.findById(id);

		// リクエスト領域に値をセット
		request.setAttribute("loginId",user.getLoginId() );
		request.setAttribute("name",user.getName() );
		request.setAttribute("birthDate",user.getBirthDate() );
		request.setAttribute("id",user.getId() );

		// フォワードだよ
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// マルチバイト文字列の文字化け対応
		request.setCharacterEncoding("UTF-8");

		// userUpdate.jspで入力した情報を取得

		// リクエストパラメーターの取得
		// URLパラメータから値を取得
		String userID = request.getParameter("id");
		int id = Integer.parseInt(userID);

		String loginID = request.getParameter("loginId");

        String name = request.getParameter("name");
        String birthDate = request.getParameter("birthDate");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String updateDate = request.getParameter("updateDate");

        /** パスワードとパスワード（確認）の入力内用が異なる場合 **/
		if(!(password.equals(password2))) {

			// 値をセット
			request.setAttribute("loginId",loginID );
			request.setAttribute("name",name );
			request.setAttribute("birthDate",birthDate );
			request.setAttribute("id",id );

			// リクエストスコープにエラーメッセージをセット
				request.setAttribute("errMsg", "入力された内容は正しくありません");

			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;

		}

		/** パスワード以外に未入力がある場合 **/
		if (name.equals("") || birthDate.equals("")) {

			// 値をセット
			request.setAttribute("loginId",loginID );
			request.setAttribute("name",name );
			request.setAttribute("birthDate",birthDate );
			request.setAttribute("id",id );

			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

        // 、Daoのメソッドを実行
     	UserDao userDao = new UserDao();

     	 // 、Daoの暗号化メソッドを実行
     	String pass = userDao.pass(password);

     	 // 、Daoの更新メソッドを実行
     	userDao.updateUser(name, pass, birthDate, updateDate ,id);



     // ユーザ一覧のサーブレットにリダイレクト
        response.sendRedirect("UserListServlet");


	}

}
