package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class DeleteServlet
 */
@WebServlet("/DeleteServlet")
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// マルチバイト文字列の文字化け対応
				request.setCharacterEncoding("UTF-8");

				// URLパラメータから値を取得
				String userID = request.getParameter("id");
				int id = Integer.parseInt(userID);

				// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
				UserDao userDao = new UserDao();
				User user = userDao.findById(id);

				// リクエスト領域に値をセット
				request.setAttribute("user",user );

				// フォワードだよ
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDelete.jsp");
				dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// マルチバイト文字列の文字化け対応
				request.setCharacterEncoding("UTF-8");

				// リクエストパラメーターの取得
				// URLパラメータから値を取得
				String userID = request.getParameter("id");
				int id = Integer.parseInt(userID);


		        // 、Daoのメソッドを実行
		     	UserDao userDao = new UserDao();
		     	userDao.delUser(id);


		     // ユーザ一覧のサーブレットにリダイレクト
		        response.sendRedirect("UserListServlet");

	}

}
