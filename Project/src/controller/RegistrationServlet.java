package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserRegistration
 */
@WebServlet("/RegistrationServlet")
public class RegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegistrationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// フォワードだよ
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userSignup.jsp");
		dispatcher.forward(request, response);


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//登録するためのやつを書く

		// リクエストパラメーターの取得文字化け
        request.setCharacterEncoding("UTF-8");

        // リクエストパラメーターの取得

        String loginId = request.getParameter("loginId");
        String name = request.getParameter("name");
        String birthDate = request.getParameter("birthDate");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String createDate = request.getParameter("createDate");
        String updateDate = request.getParameter("updateDate");

        /** 入力項目に１つでも未入力がある場合 **/
		if (loginId.equals("") || name.equals("") || birthDate.equals("") || password.equals("")) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userSignup.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** パスワードとパスワード（確認）の入力内用が異なる場合 **/
		if(!(password.equals(password2))) {
			// リクエストスコープにエラーメッセージをセット
				request.setAttribute("errMsg", "入力された内容は正しくありません");

			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userSignup.jsp");
			dispatcher.forward(request, response);
			return;

		}
		 // 、Daoのメソッドを実行
		UserDao userDao = new UserDao();
		User user = userDao.findByUserId(loginId);

		/** 既に登録されているログインIDが入力された場合 **/
		if(user != null) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

		// ログインjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userSignup.jsp");
		dispatcher.forward(request, response);
		return;
		}

		 // 、Daoの暗号化メソッドを実行
     	String pass = userDao.pass(password);

        // 、Daoのメソッドを実行
     	userDao.createUser(loginId, name, birthDate, pass, createDate, updateDate);




     // ユーザ一覧のサーブレットにリダイレクト
        response.sendRedirect("UserListServlet");


	}

}
